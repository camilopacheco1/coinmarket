//
//  ChartData.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 21/02/22.
//

import Foundation
import Charts

struct ChartData  {
    var yNames : [String] = []
    var entries: [ChartDataEntry] = []
    
    public mutating func setEntries(priceHistory: [Float]) {
        var entriesAux : [ChartDataEntry] = []
        self.yNames.removeAll()
        if !priceHistory.isEmpty{
            for i in 1...priceHistory.count {
                let currentIndex = i - 1
                let currentTime = currentIndex * 6
                let yName = String(currentTime) + " min"
                self.yNames.insert(yName, at: 0)
                let entry = ChartDataEntry(x: Double(i), y: Double(priceHistory[i - 1]))
                entriesAux.append(entry)
            }
            entries = entriesAux
        }
    }
}
