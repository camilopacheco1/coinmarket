//
//  Cryptocurrency.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 2/02/22.
//

import Foundation


public struct Cryptocurrencies: Decodable{
    let data: [Cryptocurrency]
}


public struct Cryptocurrency: Decodable, Identifiable {
    public var id: Int
    public let name: String
    public let quote: quote
}

public struct quote : Decodable{
    let USD : Usd
}

public struct Usd : Decodable{
    public var price : Float
}


