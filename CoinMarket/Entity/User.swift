//
//  User.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 6/02/22.
//

import Foundation

struct User : Codable {
    let id : String
    let email: String
    let password: String
}
