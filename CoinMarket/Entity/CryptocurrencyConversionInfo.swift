//
//  CryptocurrencyConvertionInfo.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 21/02/22.
//

import Foundation

public struct CryptocurrencyConversionInfo: Decodable{
    let data: ConversionInfo
}

public struct ConversionInfo: Decodable{
    let id : Int
    let symbol : String
    let name : String
    let amount : Int
    let quote : [String:Conversion]
}

public struct Conversion : Decodable{
    let price : Float
}

