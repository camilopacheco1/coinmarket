//
//  CryptocurrencyInfo.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 3/02/22.
//

import Foundation

struct CryptocurrencyInfoData: Decodable {
    let data : [String:CryptocurrencyInfo]
}
struct CryptocurrencyInfo: Decodable {
    var id: Double
    let name: String
    let description : String
    let logo : String
}
