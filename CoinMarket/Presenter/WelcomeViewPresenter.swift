//
//  WelcomeViewPresenter.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 4/02/22.
//

import SwiftUI
import Combine

class WelcomeViewPresenter {
    private let router = Router()
    
    func goToLoginView<Content: View>(
        rootIsActive: Binding<Bool>,
        @ViewBuilder content: () -> Content
    ) -> some View {
        NavigationLink(
            destination: router.goToLogInView(rootIsActive: rootIsActive),
            isActive: rootIsActive){
            content()
        }.isDetailLink(true)
        
    }
    func goToSignupView<Content: View>(
        rootIsActive: Binding<Bool>,
        @ViewBuilder content: () -> Content
    ) -> some View {
        NavigationLink(
            destination: router.goToSignupView(rootIsActive: rootIsActive),
            isActive: rootIsActive){
            content()
        }.isDetailLink(true)
        
    }

}
