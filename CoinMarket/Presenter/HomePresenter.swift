//
//  HomePresenter.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 2/02/22.
//

import SwiftUI
import Combine

class HomePresenter: ObservableObject {
    
    private let homeInteractor: HomeInteractor
    private var cancellables = Set<AnyCancellable>()
    @Published var cryptoCurrencies : [CryptocurrencyUI] = []
    private let router = Router()
    
    init(homeInteractor : HomeInteractor) {
        self.homeInteractor = homeInteractor
        homeInteractor.$cryptoCurrencies
            .map{
                $0.map{
                    let criptoCurrency : CryptocurrencyUI = CryptocurrencyUI(id: $0.id, name: $0.name, price: $0.quote.USD.price)
                    return criptoCurrency
                }
            }
            .assign(to: \.cryptoCurrencies, on: self)
            .store(in: &cancellables)
    }
    
    func fetchData(){
        self.homeInteractor.fetchData()
    }
    
    func goToDetailView<Content: View>(
        for cryptocurrencyId : String,
        cryptocurrencyPrice: String,
        @ViewBuilder content: () -> Content
      ) -> some View {
        NavigationLink(
            destination: router.goToDetailView(for: cryptocurrencyId, cryptocurrencyPrice: cryptocurrencyPrice)) {
              content()
        }
    }
    
    func goToPriceConversionView<Content: View>(
        criptocurrencyFrom : CryptocurrencyUI,
        @ViewBuilder content: () -> Content
      ) -> some View {
        NavigationLink(
            destination: router.goToPriceConversion(criptocurrencyFrom: criptocurrencyFrom)) {
              content()
        }
    }
}
