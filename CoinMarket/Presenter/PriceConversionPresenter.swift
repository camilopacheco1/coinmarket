//
//  PriceConversionPresenter.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 21/02/22.
//

import SwiftUI
import Combine

class PriceConversionPresenter: ObservableObject {
    
    private let priceConversionInteractor : PriceConversionInteractor
    private var cancellables = Set<AnyCancellable>()
    private var errorMessagecancellables = Set<AnyCancellable>()
    @Published var convertedPrice : String?
    @Published var errorMessage : String = ""
    private let router = Router()
    
    init(priceConversionInteractor : PriceConversionInteractor) {
        self.priceConversionInteractor = priceConversionInteractor
        priceConversionInteractor.$convertedPrice
            .map{
                let priceString = String($0)
                return priceString
            }
            .assign(to: \.convertedPrice, on: self)
            .store(in: &cancellables)
        
        priceConversionInteractor.$errorMessage
            .assign(to: \.errorMessage, on: self)
            .store(in: &errorMessagecancellables)
    }
    
    func convertPrice(cryptoCurrencyIDFrom: Int, amount: Int, cryptoCurrencySymbolTo: String ){
        let isAmountFieldValid = amount != 0
        let isCryptoCurrencySymbolToFieldValid = cryptoCurrencySymbolTo != ""
        if !isAmountFieldValid {
            self.errorMessage = "amount is invalid"
            return
        }
        if !isCryptoCurrencySymbolToFieldValid {
            self.errorMessage = "cryptocurrency symbol field is empty"
            return
        }
        self.priceConversionInteractor.convertPrice(cryptoCurrencyIDFrom: cryptoCurrencyIDFrom, amount: amount, cryptoCurrencySymbolTo: cryptoCurrencySymbolTo)
    }
}
