//
//  SignupPresenter.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 7/02/22.
//

import Combine
import SwiftUI

class SignupPresenter : ObservableObject {
    
    private var signupInteractor : SignupInteractor
    private var cancellables = Set<AnyCancellable>()
    private let router = Router()
    @Published  var user : User?
    @Published  var errorMessage : String = ""
    
    init(signupInteractor : SignupInteractor) {
        self.signupInteractor = signupInteractor
        signupInteractor.$user
            .assign(to: \.user, on: self)
            .store(in: &cancellables)
    }
    
    func goToHomeView<Content: View>(
        shouldPopToRootView: Binding<Bool>,
        isActive:Bool,
        @ViewBuilder content: () -> Content
      ) -> some View {
     NavigationLink(
        destination: router.goToHomeView(shouldPopToRootView: shouldPopToRootView),
        isActive: .constant(isActive),
        label: {
            content()
        }).isDetailLink(true)
    }
    
    func goToLoginView<Content: View>(
        rootIsActive: Binding<Bool>,
        isActive:Bool,
        @ViewBuilder content: () -> Content
      ) -> some View {
     NavigationLink(
        destination: router.goToLogInView(rootIsActive: rootIsActive),
        isActive: .constant(isActive),
        label: {
            content()
        }).isDetailLink(true)
    }
    func signUp(email : String, password: String){
        let isEmailFieldValid = email != ""
        let isPasswordFieldValid = password != ""
        if !isEmailFieldValid {
            self.errorMessage = "Email field is empty"
            return
        }
        if !isPasswordFieldValid{
            self.errorMessage = "Password field is empty"
            return
        }
         
        self.signupInteractor.saveUser(email: email, password: password)
    }
}
