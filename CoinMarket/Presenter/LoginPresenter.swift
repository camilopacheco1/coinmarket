//
//  LoginPresenter.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 6/02/22.
//

import Combine
import SwiftUI

class LoginPresenter: ObservableObject {
    
    private let loginIteractor  : LoginInteractor
    private var cancellables = Set<AnyCancellable>()
    private let router = Router()
    @Published var user : User?
    
    init(loginInteractor: LoginInteractor) {
        self.loginIteractor  = LoginInteractor()
        loginIteractor.$user.assign(to: \.user, on: self).store(in : &cancellables)
        
    }
    
    func goToHomeView<Content: View>(
        shouldPopToRootView: Binding<Bool>,
        isActive:Bool,
        @ViewBuilder content: () -> Content
      ) -> some View {
     NavigationLink(
        destination: router.goToHomeView(shouldPopToRootView: shouldPopToRootView),
        isActive: .constant(isActive),
        label: {
            content()
        }).isDetailLink(true)
    }
    
    func goToSignup<Content: View>(
        shouldPopToRootView: Binding<Bool>,
        isActive:Bool,
        @ViewBuilder content: () -> Content
      ) -> some View {
     NavigationLink(
        destination: router.goToSignupView(rootIsActive: shouldPopToRootView),
        isActive: .constant(isActive),
        label: {
            content()
        }).isDetailLink(true)
    }
    
    func validateLogin(email: String, password: String) -> Bool{
        let user = self.loginIteractor.getUserInfo(email: email, password: password)
        let isValidUser = user.email == email && user.password == password
        return isValidUser
    }
    
    
}
