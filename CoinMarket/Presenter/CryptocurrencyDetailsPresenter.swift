//
//  CryptocurrencyDetailsPresenter.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 4/02/22.
//

import Combine

class CryptocurrencyDetailsPresenter: ObservableObject {
    private let cryptocurrencyDetailsInteractor : CryptocurrencyDetailsInteractor
    private var cryptoCurrencyInfoCancellables = Set<AnyCancellable>()
    private var priceHistoryCancellables = Set<AnyCancellable>()
    @Published var cryptoCurrencyInfo : CryptocurrencyInfo?
    @Published var chartInfo : ChartData?
    
    init(cryptocurrencyDetailsInteractor : CryptocurrencyDetailsInteractor) {
        self.cryptocurrencyDetailsInteractor = cryptocurrencyDetailsInteractor
        cryptocurrencyDetailsInteractor.$cryptoCurrencyInfo
            .assign(to: \.cryptoCurrencyInfo, on: self)
            .store(in: &cryptoCurrencyInfoCancellables)
        
        cryptocurrencyDetailsInteractor.$priceHistory
            .map({
                var chartData = ChartData()
                chartData.setEntries(priceHistory: $0)
                return chartData
            })
            .assign(to: \.chartInfo, on: self)
            .store(in: &priceHistoryCancellables)
        
    }
    
    func getCryptoCurrencyInfo() {
        cryptocurrencyDetailsInteractor.getCryptoCurrencyInfo()
    }
    
    func getChartInfo(){
        cryptocurrencyDetailsInteractor.getCryptocurrencyPriceHIstory()
    }
}
