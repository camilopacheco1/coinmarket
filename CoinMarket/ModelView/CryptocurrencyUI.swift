//
//  CryptocurrencyUI.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 10/02/22.
//

import Foundation

public struct CryptocurrencyUI: Decodable, Identifiable {
    public var id: Int
    public let name: String
    public let price: Float
}
