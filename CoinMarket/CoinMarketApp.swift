//
//  CoinMarketApp.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 2/02/22.
//

import SwiftUI

@main
struct CoinMarketApp: App {
    var body: some Scene {
        WindowGroup {
            WelcomeView()
        }
    }
}
