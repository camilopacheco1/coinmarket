//
//  HomeRouter.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 4/02/22.
//

import SwiftUI

class Router {
    
    func goToDetailView(for cryptocurrencyId : String, cryptocurrencyPrice : String) -> some View {
        let iterator = CryptocurrencyDetailsInteractor(cryptocurrencyId: cryptocurrencyId)
        let presenter = CryptocurrencyDetailsPresenter(cryptocurrencyDetailsInteractor: iterator)
        return CryptocurrencyDetailsView(cryptocurrencyDetailsPresenter: presenter,cryptocurrencyPrice: cryptocurrencyPrice)
    }
    
    func goToHomeView(shouldPopToRootView: Binding<Bool>) -> some View {
        let homeInteractor = HomeInteractor()
        let homePresenter = HomePresenter(homeInteractor: homeInteractor)
        return HomeView(homePresenter: homePresenter, shouldPopToRootView: shouldPopToRootView)
    }
    
    func goToLogInView(rootIsActive:Binding<Bool> ) -> some View {
        let loginInteractor = LoginInteractor()
        let loginPresenter = LoginPresenter(loginInteractor: loginInteractor)
        return LoginView(loginPresenter: loginPresenter, rootIsActive: rootIsActive)
    }
    
    func goToSignupView(rootIsActive:Binding<Bool>) -> some View {
        let interactor = SignupInteractor()
        let presenter = SignupPresenter(signupInteractor: interactor)
        return SignupView(signupPresenter: presenter, rootIsActive: rootIsActive)
    }
    
    func goToPriceConversion(criptocurrencyFrom : CryptocurrencyUI) -> some View {
        let interactor = PriceConversionInteractor()
        let presenter = PriceConversionPresenter(priceConversionInteractor: interactor)
        return PriceConversionView(priceConversionPresenter: presenter, criptocurrencyFrom: criptocurrencyFrom)
    }
}
