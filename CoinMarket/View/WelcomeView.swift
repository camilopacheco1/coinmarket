//
//  WelcomeView.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 4/02/22.
//

import SwiftUI

struct WelcomeView: View {
    let welcomeViewPresenter = WelcomeViewPresenter()
    @State var isLoginLinkActive : Bool = false
    @State var isSignupLinkActive : Bool = false
    var body: some View {
        NavigationView{
            VStack{
                Image("coinMarketIcon")
                Text("Welcome to \n Coin Market")
                    .font(.system(.title, design: .rounded))
                welcomeViewPresenter.goToLoginView(rootIsActive: self.$isLoginLinkActive){
                    Button(action: {
                        self.isLoginLinkActive = true
                    }, label: {
                        Text("Sign In")
                    }).buttonStyle(StylesAndComponents.roundedRectangleButtonStyle())
                    .frame(width: 200, height: 100, alignment: .bottom)
                }
                welcomeViewPresenter.goToSignupView(rootIsActive: self.$isSignupLinkActive){
                    Button(action: {
                        self.isSignupLinkActive = true
                    }, label: {
                        Text("Sign Up")
                    }).buttonStyle(StylesAndComponents.roundedRectangleButtonStyle())
                    .frame(width: 200, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                }
            }
        }
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}

