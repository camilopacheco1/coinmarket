//
//  LoginView.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 6/02/22.
//

import SwiftUI
import ActivityIndicatorView

struct LoginView: View {
    @ObservedObject var loginPresenter : LoginPresenter
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var message: String = ""
    @State private var moveToHomeView : Bool = false
    @State private var moveToSignupView : Bool = false
    @State private var showLoadingIndicator : Bool = false
    @Binding var rootIsActive : Bool
    
    let errorMessage = "Email or password Incorrect"
    var body: some View {
        ZStack{
            VStack{
                Image("coinMarketIcon").resizable()
                    .frame(width: 70, height: 70)
                Text("Coin Master").foregroundColor(.gray)
                VStack(alignment: .leading){
                    Text("Sign In").font(.title).bold()
                    StylesAndComponents.alertMessage(message: self.message)
                    Text("Email")
                    TextField("Your email address", text: $email)
                    Text("Password")
                    SecureField("your password", text: $password)
                }.padding()
                .textFieldStyle(StylesAndComponents.underlineTextFieldStyle())
                .autocapitalization(.none)
                loginPresenter.goToHomeView(shouldPopToRootView: self.$rootIsActive, isActive: self.moveToHomeView){
                    Button(action: {
                        let currentEmail = email
                        let isValidUser = self.loginPresenter.validateLogin(email: currentEmail, password: self.password)
                        if isValidUser{
                            self.showLoadingIndicator = true
                        }else{
                            self.message = errorMessage
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                            
                            if isValidUser {
                                self.showLoadingIndicator = false
                                UIActivityIndicatorView(style: .medium).stopAnimating()
                                self.moveToHomeView = true
                            }
                            self.message = ""
                        }
                    }, label: {
                        Text("Sign In")
                    }).buttonStyle(StylesAndComponents.roundedRectangleButtonStyle())
                    .frame(width: 300, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                }
                self.loginPresenter.goToSignup(shouldPopToRootView: self.$rootIsActive, isActive: self.moveToSignupView){
                    Button(action: {
                        self.moveToSignupView = true
                    }, label: {
                        Text("Sign Up").foregroundColor(.black).bold()
                    })
                }
                Spacer(minLength: 175)
            }
            ActivityIndicatorView(isVisible: $showLoadingIndicator, type: .rotatingDots).foregroundColor(.black).frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .center)
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        let interactor = LoginInteractor()
        let presenter = LoginPresenter(loginInteractor: interactor)
        Utils.StatefulPreviewWrapper(false) {LoginView(loginPresenter: presenter, rootIsActive: $0)}
    }
}
