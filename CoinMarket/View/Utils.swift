//
//  Utils.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 8/02/22.
//

import Foundation
import SwiftUI

class Utils {
    enum CryptocurrencyCategory {
        case moreGains
        case moreLoses
        case none
    }
    struct StatefulPreviewWrapper<Value, Content: View>: View {
        @State var value: Value
        var content: (Binding<Value>) -> Content

        var body: some View {
            content($value)
        }

        init(_ value: Value, content: @escaping (Binding<Value>) -> Content) {
            self._value = State(wrappedValue: value)
            self.content = content
        }
    }
    
    public func filterCryptocurrency(cryptoCurrencies : [CryptocurrencyUI], searchText: String, category: CryptocurrencyCategory) -> [CryptocurrencyUI] {
        let isFilterBySearchText = !searchText.isEmpty
        let isFileterByCategory = category != .none
        var cryptoCurrenciesFilter = cryptoCurrencies
        if isFilterBySearchText{
            cryptoCurrenciesFilter = cryptoCurrenciesFilter.filter({ $0.name.contains(searchText) })
        }
        if isFileterByCategory {
            if category == .moreGains{
                cryptoCurrenciesFilter.sort(by: {
                    $0.price > $1.price
                })
            }
            if category == .moreLoses{
                cryptoCurrenciesFilter.sort(by: {
                    $0.price < $1.price
                })
            }
        }
        return cryptoCurrenciesFilter
    }
    
    public func loadImage(for urlString: String, completionHandler: @escaping (Data) -> Void) {
        guard let url = URL(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            completionHandler(data)
        }
        task.resume()
    }
}
