//
//  SignupView.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 7/02/22.
//

import SwiftUI
import ActivityIndicatorView

struct SignupView: View {
    @ObservedObject var signupPresenter : SignupPresenter
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var message: String = ""
    @State private var moveToHomeView : Bool = false
    @State private var moveToSigninView : Bool = false
    @State private var showLoadingIndicator : Bool = false
    @Binding var rootIsActive : Bool
    
    let errorMessage = "Email or password Incorrect"
    var body: some View {
        ZStack{
            VStack{
                Image("coinMarketIcon").resizable()
                    .frame(width: 70, height: 70)
                Text("Coin Master").foregroundColor(.gray)
                VStack(alignment: .leading){
                    Text("Sign Up").font(.title).bold()
                    StylesAndComponents.alertMessage(message: self.message)
                    Text("Email")
                    TextField("Your email address", text: $email)
                    Text("Password")
                    SecureField("your password", text: $password)
                }.padding()
                .textFieldStyle(StylesAndComponents.underlineTextFieldStyle())
                .autocapitalization(.none)
                signupPresenter.goToHomeView(shouldPopToRootView: self.$rootIsActive, isActive: self.moveToHomeView){
                    Button(action: {
                        self.signupPresenter.signUp(email: self.email, password: self.password)
                        let isValidUser = self.signupPresenter.user != nil
                        if isValidUser{
                            self.showLoadingIndicator = true
                        }else{
                            self.message = self.signupPresenter.errorMessage
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                            
                            if isValidUser {
                                self.showLoadingIndicator = false
                                UIActivityIndicatorView(style: .medium).stopAnimating()
                                self.moveToHomeView = true
                            }
                            self.message = ""
                        }
                    }, label: {
                        Text("Sign Up")
                    }).buttonStyle(StylesAndComponents.roundedRectangleButtonStyle())
                    .frame(width: 300, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                }
                HStack{
                    Text("have an account? ")
                    signupPresenter.goToLoginView(rootIsActive: self.$rootIsActive, isActive: self.moveToSigninView){
                        Button(action: {
                            self.moveToSigninView = true
                        }, label: {
                            Text("Sign In")
                                .foregroundColor(.black).bold()
                        })
                    }
                    
                }
                Spacer(minLength: 175)
                
            }
            ActivityIndicatorView(isVisible: $showLoadingIndicator, type: .rotatingDots).foregroundColor(.black).frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .center)
        }
    }
}

struct SignupView_Previews: PreviewProvider {
    static var previews: some View {
        let interactor = SignupInteractor()
        let presenter = SignupPresenter(signupInteractor: interactor)
        Utils.StatefulPreviewWrapper(false) {
            SignupView(signupPresenter: presenter, rootIsActive: $0)
        }
        
    }
}
