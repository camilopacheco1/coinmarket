//
//  ContentView.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 2/02/22.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject var homePresenter : HomePresenter
    @State var searchText: String = ""
    @State var filterCategory :  Utils.CryptocurrencyCategory = .none
    @Binding var shouldPopToRootView : Bool
    
    var body: some View {
        VStack{
            TextField("Search ...", text: $searchText)
                .padding(7)
                .padding(.horizontal, 25)
                .background(Color(.systemGray6))
                .cornerRadius(8)
                .overlay(
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.gray)
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 8)
                        Menu {
                            Button {
                                filterCategory = .moreLoses
                            } label: {
                                Text("More Loses")
                                Image(systemName: "arrow.down.right.circle")
                            }
                            Button {
                                filterCategory = .moreGains
                            } label: {
                                Text("More Gains")
                                Image(systemName: "arrow.up.right.circle")
                            }
                        } label: {
                            Image(systemName: "list.bullet").padding().foregroundColor(.black)
                        }
                    }
                )
                .padding(.horizontal, 10)
                .padding()
            let utils = Utils()
            let cryptoCurrenciesFilter = utils.filterCryptocurrency(cryptoCurrencies: homePresenter.cryptoCurrencies, searchText: self.searchText, category: self.filterCategory)
            HStack{
                List(cryptoCurrenciesFilter){cryptocurrency in
                    let roundedPrice = round(cryptocurrency.price * 100) / 100.0
                    ZStack{
                        homePresenter.goToDetailView(for: String(cryptocurrency.id), cryptocurrencyPrice: String(roundedPrice)){
                            EmptyView()
                        }.hidden()
                        Text(cryptocurrency.name).padding()
                    }
                    
                }
                List(cryptoCurrenciesFilter){cryptocurrency in
                    ZStack{
                        homePresenter.goToPriceConversionView(
                            criptocurrencyFrom :cryptocurrency
                        ){
                            EmptyView()
                        }.hidden()
                        Text("Convert")
                            .padding()
                            .background(Color.black.cornerRadius(8))
                            .foregroundColor(.white)
                            .font(.headline)
                    }
                }
            }
        }
        .toolbar(content: {
            Button(action: {
                self.shouldPopToRootView = false
            }, label: {
                Text("Log Out")
            })
        })
        .navigationBarTitle("CryptoCurrency", displayMode: .inline)
        .navigationBarBackButtonHidden(true)
        .onAppear(perform: {
            self.homePresenter.fetchData()
        })
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        let homeInteractor = HomeInteractor()
        let homePresenter = HomePresenter(homeInteractor: homeInteractor)
        Utils.StatefulPreviewWrapper(false) {
            HomeView(homePresenter: homePresenter, shouldPopToRootView: $0)
        }
    }
}
