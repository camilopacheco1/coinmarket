//
//  Styles&Components.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 6/02/22.
//

import SwiftUI

class StylesAndComponents {
    struct roundedRectangleButtonStyle: ButtonStyle {

        func makeBody(configuration: Configuration) -> some View {
        HStack {
          Spacer()
          configuration.label.foregroundColor(.white)
          Spacer()
        }
        .padding()
        .background(Color.black.cornerRadius(8))
        .scaleEffect(configuration.isPressed ? 0.95 : 1)
      }
    }

    struct underlineTextFieldStyle: TextFieldStyle {
      func _body(configuration: TextField<_Label>) -> some View {
        configuration
            .cornerRadius(5.0)
            .underlineTextField()
            
      }
    }
    
    struct alertMessage : View {
        var message : String = ""
        var body: some View{
            Text(self.message)
                .padding()
                .background(Color.red)
                .foregroundColor(.black)
                .font(.headline)
                .cornerRadius(10.0)
                .opacity(self.message != ""  ? 1 : 0)
            
        }
    }
}

extension View {
    func underlineTextField() -> some View {
        self
            .overlay(Rectangle().frame(height: 2).padding(.top, 35))
            .foregroundColor(.gray)
            .padding(10)
    }
}
