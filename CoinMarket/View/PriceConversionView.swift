//
//  PriceConversionView.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 21/02/22.
//

import SwiftUI
import Combine

struct PriceConversionView: View {
    @ObservedObject var priceConversionPresenter : PriceConversionPresenter
    @State private var amount: String = ""
    @State private var criptocurrencyTo: String = ""
    var criptocurrencyFrom : CryptocurrencyUI?
    var body: some View {
        VStack{
            Image("coinMarketIcon").resizable()
                .frame(width: 70, height: 70)
            Text("Coin Master").foregroundColor(.gray)
            VStack(alignment: .leading){
                Text("Price Conversion").font(.title).bold()
                StylesAndComponents.alertMessage(message: priceConversionPresenter.errorMessage)
                Text("From:  \(criptocurrencyFrom?.name ?? "")").font(.title2).bold().frame(height: 80)
                Text("Amount:").font(.title2).bold()
                TextField("Amount to Convert", text: $amount)
                Text("To:").font(.title2).bold()
                TextField("Convert Cryptocurrency To", text: $criptocurrencyTo)
                Text(priceConversionPresenter.convertedPrice ?? "0").font(.title3).bold().padding()
            }.padding()
            .textFieldStyle(StylesAndComponents.underlineTextFieldStyle())
            .autocapitalization(.none)
            Button(action: {
                let id = self.criptocurrencyFrom?.id ?? 1
                let amount = Int(self.amount) ?? 0
                self.priceConversionPresenter.convertPrice(cryptoCurrencyIDFrom: id, amount: amount, cryptoCurrencySymbolTo: self.criptocurrencyTo)
            }, label: {
                Text("Convert")
            }).buttonStyle(StylesAndComponents.roundedRectangleButtonStyle())
            .frame(width: 120, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            Text("qweqwe")

        }
    }
}

struct PriceConversionView_Previews: PreviewProvider {
    static var previews: some View {
        let interactor = PriceConversionInteractor()
        let presenter = PriceConversionPresenter(priceConversionInteractor: interactor)
        let cryptocurrencyUI = CryptocurrencyUI(id: 1, name: "BitCoin", price: 123.56)
        PriceConversionView(priceConversionPresenter: presenter, criptocurrencyFrom: cryptocurrencyUI)
    }
}
