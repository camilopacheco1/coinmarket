//
//  CryptocurrencyDetailsView.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 4/02/22.
//

import SwiftUI
import Combine
import Charts

struct CryptocurrencyDetailsView: View {
    @ObservedObject var cryptocurrencyDetailsPresenter : CryptocurrencyDetailsPresenter
    @State private var showingAlert = false
    var cryptocurrencyPrice: String
    @ObservedObject var imageLoader = ImageLoaderService()
    @State var image: UIImage = UIImage()
    let timer = Timer.publish(every: 60, on: .main, in: .common).autoconnect()
    
    var body: some View {
        let currencyInfo = cryptocurrencyDetailsPresenter.cryptoCurrencyInfo
        let chartInfo = cryptocurrencyDetailsPresenter.chartInfo!
        VStack(alignment: .center, spacing: nil){
            Image(uiImage: image)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width:100, height:100)
                .onReceive(cryptocurrencyDetailsPresenter.$cryptoCurrencyInfo) { cryptoCurrencyInfo in
                    let util = Utils()
                    util.loadImage(for: cryptoCurrencyInfo?.logo ?? ""){data in
                        self.image = UIImage(data: data)!
                    }
                }
            Text(currencyInfo?.name ?? "").font(.title).bold()
            if chartInfo.yNames.count != 0 && chartInfo.yNames.count == chartInfo.entries.count{
                MultiLineChartView(entries1: chartInfo.entries, days: chartInfo.yNames, numberOfY: chartInfo.yNames.count).frame(height: 220)
            }
            Text(currencyInfo?.description ?? "").lineLimit(nil).padding()
            Button(action: {
                showingAlert = true
            }, label: {
                Text("Current Value")
            })
            .buttonStyle(StylesAndComponents.roundedRectangleButtonStyle())
            .frame(width: 300, height: 100, alignment:.center)
            .alert(isPresented: $showingAlert, content: {
                Alert(title: Text("Current Value"), message: Text(self.cryptocurrencyPrice + " USD"), dismissButton:  .default(Text("OK")))
            })
            Spacer()
        }
        .onAppear(perform: {
            cryptocurrencyDetailsPresenter.getCryptoCurrencyInfo()
            cryptocurrencyDetailsPresenter.getChartInfo()
        }).padding()
        .onReceive(timer) { input in           cryptocurrencyDetailsPresenter.getChartInfo()
        }
    }
}

struct CryptocurrencyDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        let id = ""
        let iterator = CryptocurrencyDetailsInteractor(cryptocurrencyId: id)
        let presenter = CryptocurrencyDetailsPresenter(cryptocurrencyDetailsInteractor: iterator)
        CryptocurrencyDetailsView(cryptocurrencyDetailsPresenter: presenter, cryptocurrencyPrice: "0")
    }
}
