//
//  signUpInteractor.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 7/02/22.
//

import Foundation

class SignupInteractor {
    @Published var user : User?
    
    func saveUser(email : String, password: String){
        let randomInt = Int.random(in: 0..<6)
        let id = String(randomInt)
        self.user = User(id: id, email: email, password: password)
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(self.user)
            UserDefaults.standard.set(data, forKey: "user")

        } catch {
            print("Unable to Encode Note (\(error))")
        }
    }
}
