//
//  PriceConversionInteractor.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 21/02/22.
//

import Combine
import Foundation

class PriceConversionInteractor {
    private var cancellable: AnyCancellable?
    @Published var convertedPrice : Float = 0.0
    @Published var errorMessage : String = ""
    
    func convertPrice(cryptoCurrencyIDFrom: Int, amount: Int, cryptoCurrencySymbolTo: String ) {
        
        let url = URL(string: "https://pro-api.coinmarketcap.com/v2/tools/price-conversion?amount=\(amount)&id=\(cryptoCurrencyIDFrom)&convert=\(cryptoCurrencySymbolTo)")
        var request = URLRequest(url: url!)
        request.allHTTPHeaderFields = [
            "X-CMC_PRO_API_KEY": "88c14035-d067-477c-b9b4-e36d9b2a79a8"
        ]
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        cancellable = URLSession.shared.dataTaskPublisher(for: request)
            .map({
                return $0.data
            })
            .decode(type: CryptocurrencyConversionInfo.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                    switch completion {
                    case .failure(let error):
                        print("Error \(error)")
                        self.errorMessage = "cryptocurrency symbol field is invalid"
                        self.convertedPrice = 0
                    case .finished:
                        self.errorMessage = ""
                        print("Publisher is finished")
                    } },
                  receiveValue: {
                    self.convertedPrice = $0.data.quote.first?.value.price ?? 0.0
                  })
    }
    
    deinit {
        cancellable?.cancel()
    }
    
}
