//
//  CryptocurrencyDetailsInteractor.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 3/02/22.
//

import Combine
import Foundation

class CryptocurrencyDetailsInteractor {
    
    @Published var cryptoCurrencyInfo : CryptocurrencyInfo?
    @Published var priceHistory : [Float] = []
    private var cryptoCurrencyInfoCancellable: AnyCancellable?
    private var priceHistoryCancellable: AnyCancellable?
    var cryptocurrencyId : String = ""
    
    init (cryptocurrencyId: String) {
        self.cryptocurrencyId = cryptocurrencyId
    }
    
    func getCryptoCurrencyInfo(){
        let url = URL(string: "https://pro-api.coinmarketcap.com/v2/cryptocurrency/info?id=\(self.cryptocurrencyId)")
        var request = URLRequest(url: url!)
        request.allHTTPHeaderFields = [
            "X-CMC_PRO_API_KEY": "88c14035-d067-477c-b9b4-e36d9b2a79a8"
        ]
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        cryptoCurrencyInfoCancellable = URLSession.shared.dataTaskPublisher(for: request)
            .map({
                return $0.data
            })
            .decode(type: CryptocurrencyInfoData.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { print ("Received completion: \($0).") },
                  receiveValue: {
                    if let currencyData = $0.data["\(self.cryptocurrencyId)"] {
                        self.cryptoCurrencyInfo = currencyData
                    }
                  })
        
    }
    
    func getCryptocurrencyPriceHIstory() {
        let url = URL(string: "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?limit=6")
        var request = URLRequest(url: url!)
        request.allHTTPHeaderFields = [
            "X-CMC_PRO_API_KEY": "88c14035-d067-477c-b9b4-e36d9b2a79a8"
        ]
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        priceHistoryCancellable = URLSession.shared.dataTaskPublisher(for: request)
            .map({
                return $0.data
            })
            .decode(type: Cryptocurrencies.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { print ("Received completion: \($0).") },
                  receiveValue: {
                    for cryptoCurrency in $0.data {
                        let price = cryptoCurrency.quote.USD.price
                        let key = cryptoCurrency.name
                        var currencyPrices = UserDefaults.standard.array(forKey: cryptoCurrency.name) as? [Float] ?? []
                        if currencyPrices.count > 5 {
                            currencyPrices.removeFirst()
                        }
                        currencyPrices.append(price)
                        if currencyPrices.count == 1 {
                            currencyPrices.append(price)
                        }
                        if String(cryptoCurrency.id) == self.cryptocurrencyId{
                            self.priceHistory = currencyPrices
                        }
                        UserDefaults.standard.set(currencyPrices, forKey: key)
                    }
                  })
    }
}
