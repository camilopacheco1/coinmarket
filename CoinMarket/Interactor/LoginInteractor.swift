//
//  LoginInteractor.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 6/02/22.
//

import Foundation

class LoginInteractor {
    @Published var user : User?
    
    func getUserInfo(email : String, password: String) -> User{
        if let data = UserDefaults.standard.data(forKey: "user") {
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(User.self, from: data)
                return user
            } catch {
                print("Unable to Decode Note (\(error))")
            }
        }
        return User(id: "", email: "", password: "")
    }
}
