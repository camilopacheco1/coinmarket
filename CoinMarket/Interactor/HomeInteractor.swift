//
//  HomeInteractor.swift
//  CoinMarket
//
//  Created by Camilo Pacheco on 2/02/22.
//

import Combine
import Foundation

class HomeInteractor {
    private var cancellable: AnyCancellable?
    @Published var cryptoCurrencies : [Cryptocurrency] = []
    
    func fetchData() {
        
        let url = URL(string: "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?limit=6")
        var request = URLRequest(url: url!)
        request.allHTTPHeaderFields = [
            "X-CMC_PRO_API_KEY": "88c14035-d067-477c-b9b4-e36d9b2a79a8"
        ]
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        cancellable = URLSession.shared.dataTaskPublisher(for: request)
            .map({
                return $0.data
            })
            .decode(type: Cryptocurrencies.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { print ("Received completion: \($0).") },
                  receiveValue: {
                    self.cryptoCurrencies = $0.data
                  })
    }
    
    deinit {
        cancellable?.cancel()
    }
    
}
